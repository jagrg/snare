# Sight-reading studies for percussion

Rhything, a portmanteau of the words rhythm and reading, is a
performant sight-reading web application for generating sheet music
composed on-demand using the Markov chain model.

[Live demo](https://jagrg.gitlab.io/rhything/)

## Getting Started

Clone the repository:

```sh
git clone git@git.sr.ht:~jagrg/rhything
```

Install dependencies:

```sh
npm install
```

Start the development server:

```sh
npm run dev
```

## Contributing

If you'd like to contribute to the project, please follow these steps:

1. Fork the repository.
2. Create a new branch: `git checkout -b new-feature`.
3. Commit your changes: `git commit -m 'Add new feature'`.
4. Push to your branch: `git push origin new-feature`.
5. Send me your patch.

When submitting a bug report, please describe the issue clearly and
the steps to reproduce it. If possible, also include all relevant
console information or error messages. You can find these in your
browser's developer tools.
