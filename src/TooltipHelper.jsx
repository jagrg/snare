// TooltipHelper

import React from "react";
import Tooltip from "@mui/material/Tooltip";

const TooltipHelper = ({ title, children, placement }) => {
  return (
    <Tooltip title={title} placement={placement} arrow>
      {children}
    </Tooltip>
  );
};

export default TooltipHelper;
