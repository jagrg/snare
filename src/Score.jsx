// Score

import React, { useEffect, useState, forwardRef } from "react";
import * as TotalSerialism from "total-serialism/build/ts.es5.js";
import ABCJS from "abcjs";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import "@fortawesome/fontawesome-free/css/all.css";

import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import Levels from "./Levels";

const Score = forwardRef(
  (
    {
      numberOfBars,
      numberOfBeats,
      includeBarNumbers,
      onPrint,
      zoomLevel,
      staffNumberValue,
    },
    ref,
  ) => {
    const [generatedSequence, setGeneratedSequence] = useState([]);
    const [currentScoreIndex, setCurrentScoreIndex] = useState(5 - 1); // Level 3
    const [loading, setLoading] = useState(true); // New loading state
    const [levelChangedManually, setLevelChangedManually] = useState(false);

    // TODO: make staffWidth responsive
    /* const isPortrait = window.matchMedia("(orientation: portrait)").matches; */

    const handlePrint = () => {
      if (onPrint) {
        onPrint();
      }
    };

    const handleNextClick = () => {
      // Extract values using map
      const pattern = Levels[Object.keys(Levels)[currentScoreIndex]].map(
        (entry) => entry.value,
      );

      // Create a MarkovChain instance with initial state
      const markov = new TotalSerialism.Stochastic.MarkovChain(
        pattern,
        2,
        [1, 2],
      );

      // Generate an array with 6 bars of 4/4
      const sequence = markov.chain(numberOfBeats * numberOfBars);

      // Update the state with the generated sequence and set loading to false
      setGeneratedSequence(sequence);
      setLoading(false);
      // Reset the manual change flag
      setLevelChangedManually(false);
    };

    const handleLevelChange = (event) => {
      // Change the current score index based on the selected level
      setCurrentScoreIndex(event.target.value);
      // Set the manual change flag
      setLevelChangedManually(true);
      // Set loading to true when level changes
      setLoading(true);
    };

    useEffect(() => {
      // Set loading to true when the effect starts
      setLoading(true);

      const pattern = Levels[Object.keys(Levels)[currentScoreIndex]].map(
        (entry) => entry.value,
      );

      const markov = new TotalSerialism.Stochastic.MarkovChain(
        pattern,
        2,
        [1, 2],
      );

      // 6 bars of 4/4
      const sequence = markov.chain(numberOfBeats * numberOfBars);
      setGeneratedSequence(sequence);
      // Reset loading to false once the sequence is generated
      setLoading(false);
    }, [currentScoreIndex, numberOfBars, numberOfBeats]);

    useEffect(() => {
      // Check if loading is true before rendering
      if (loading) {
        return;
      }

      const generatedABC = generatedSequence.map((value, index) => {
        const matchingEntry = Levels[
          Object.keys(Levels)[currentScoreIndex]
        ].find((entry) => entry.value === value);
        const note = matchingEntry ? matchingEntry.key : "";

        // Check if it's the last index and add the final bar line
        if (index === generatedSequence.length - 1) {
          return `${note} |]`;
        } else {
          return (index + 1) % numberOfBeats === 0 ? `${note} |` : note;
        }
      });

      /* FIXME: Currently all matches are being replaced. Maybe split
      string to get more variability */

      // Post-process to replace adjacent rests
      const transformSequence = (sequence) => {
        // Generate 0 or 1 randomly
        const randomNumber = Math.floor(Math.random() * 2);

        const replacements = [
          /* Replace adjacent Rests */
          { condition: true, pattern: /z2\sz2/g, replacement: "z4" },
          { condition: true, pattern: /z2\szc/g, replacement: "z3 c" },
          { condition: true, pattern: /z4\sz2/g, replacement: "z6" },
          { condition: true, pattern: /z4\sz4/g, replacement: "z8" },

          // Replace adjacent 1/8-notes, but skip 1/8-note triplets
          {
            condition: true,
            pattern: /cc\s(?!.*\d+cc)cc /g,
            replacement: "cccc ",
          },

          // Rolls
          {
            condition:
              Math.floor(Math.random() * 3) === 1 && currentScoreIndex + 1 >= 5,
            pattern: /c2\sc2|c\/2Lc\/2c\/2c\/2\sc\/2c\/2c/g,
            // 9-stroke roll
            replacement: "(Rc2c2)",
          },

          // Diddles
          {
            condition:
              Math.floor(Math.random() * 5) === 1 && currentScoreIndex + 1 >= 5,
            pattern: /c\/2c\/2c\/2c\/2/g,
            replacement: "Yc/2c/2c/2c/2",
          },

          {
            condition:
              Math.floor(Math.random() * 5) === 1 && currentScoreIndex + 1 >= 5,
            pattern: /cc\/2c\/2/g,
            replacement: "cYc/2Yc/2",
          },

          // Conditional ties
          {
            condition: Math.floor(Math.random() * 7) === 1,
            pattern: /z\/2c\/2c\/2c\/2/g,
            replacement: "z/2(c/2c/2)c/2",
          },

          // Conditional crescendo
          {
            // Replace if only if randomNumber is even
            condition: randomNumber % 2 === 0 && currentScoreIndex + 1 >= 5,
            pattern: /z\/2c\/2c\/2c\/2 c\/2c\/2\{\\c\}c/,
            replacement:
              "z/2!crescendo(!c/2c/2c/2 c/2c/2{\\c}!crescendo)!!ff!c",
          },

          /* Conditional diminuendo */
          {
            condition: randomNumber % 2 === 0 && currentScoreIndex + 1 >= 5,
            pattern: /c\/2c\/2c\/2c\/2 c\/2c\/2c/,
            replacement: "!diminuendo(!c/2c/2c/2c/2 c/2c/2!diminuendo)!!pp!c",
          },
        ];

        let processedSequence = sequence;

        replacements.forEach(({ condition, pattern, replacement }) => {
          if (condition) {
            processedSequence = processedSequence.replace(pattern, replacement);
          }
        });

        return processedSequence;
      };

      const inputSequence = generatedABC.join(" ");
      const processedSequence = transformSequence(inputSequence);

      // Add dynamics every N bars
      const addDynamics = (notes, dynamics, numberOfDynamics) => {
        const bars = notes.split("|").map((bar) => bar.trim());
        const barsWithDynamics = [...bars];

        const barsBetweenDynamics = Math.floor(bars.length / numberOfDynamics);
        let dynamicIndex = 0;

        for (let i = 0; i < bars.length - 1; i++) {
          if (
            i % barsBetweenDynamics === 0 &&
            dynamicIndex < numberOfDynamics
          ) {
            const dynamicToAdd = dynamics[dynamicIndex];
            const firstNoteIndex = bars[i].indexOf("c");
            if (firstNoteIndex !== -1) {
              // Find the index of the first space character before "c"
              const spaceIndex = bars[i].lastIndexOf(" ", firstNoteIndex);
              // If there is a space character before "c", insert
              // dynamicToAdd before it, otherwise add it at the
              // beginning
              const insertIndex = spaceIndex !== -1 ? spaceIndex : 0;
              barsWithDynamics[i] =
                bars[i].substring(0, insertIndex) +
                dynamicToAdd +
                bars[i].substring(insertIndex);
            }

            dynamicIndex++;
          }
        }

        // Handle the last bar separately to keep the final bar line
        const lastBarIndex = bars.length - 1;
        const dynamicToAdd = dynamics[dynamicIndex];
        const firstNoteIndex = bars[lastBarIndex].indexOf("c");
        if (firstNoteIndex !== -1) {
          const spaceIndex = bars[lastBarIndex].lastIndexOf(
            " ",
            firstNoteIndex,
          );
          const insertIndex = spaceIndex !== -1 ? spaceIndex : 0;
          barsWithDynamics[lastBarIndex] =
            bars[lastBarIndex].substring(0, insertIndex) +
            dynamicToAdd +
            bars[lastBarIndex].substring(insertIndex) +
            "c";
        }

        const result = barsWithDynamics.join("|");
        return result;
      };

      const shuffle = (array) => {
        for (let i = array.length - 1; i > 0; i--) {
          const j = Math.floor(Math.random() * (i + 1));
          [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
      };

      const dynamics = shuffle(["!p!", "!mp!", "!mf!", "!f!"]);
      const numberOfDynamics = Math.floor(Math.random() * dynamics.length) + 1;

      /* Add dynamics starting from level 5 */
      let result;

      if (currentScoreIndex + 1 >= 5) {
        result = addDynamics(processedSequence, dynamics, numberOfDynamics);
      } else {
        result = processedSequence;
      }

      // console.log(generatedABC.join(" "));
      // console.log(processedSequence);
      // console.log(result);

      let abc = `
X:1
M:${numberOfBeats}/4
L:1/8
V: all stem=up
${staffNumberValue ? "K:C perc stafflines=1" : "K:C perc"}
U: R = ///
U: r = //
U: Y = /
%%stretchlast
${result}`;

      // Add bar numbers from checkbox state
      // BUG? Small font-size lowers the bar number
      if (includeBarNumbers) {
        abc += "\n%%barnumbers 1";
      }
      const preferredMeasuresPerLine = currentScoreIndex + 1 >= 4 ? 4 : 2;

      ABCJS.renderAbc("markov", abc, {
        responsive: "resize",
        selectionColor: "#9C005C",
        staffwidth: 720 * zoomLevel,
        add_classes: true,
        wrap: {
          minSpacing: 1.8,
          maxSpacing: 2.7,
          preferredMeasuresPerLine: preferredMeasuresPerLine,
        },
      });

      // Cleanup the event listener when the component unmounts
      const handleKeyDown = (event) => {
        // Check if the Enter key was pressed
        if (event.key === "Enter" || event.key === "Return") {
          // Prevent handling if the event originated from the Select component
          if (!event.target.classList.contains("level")) {
            handleNextClick();
          }
        }
      };

      window.addEventListener("keydown", handleKeyDown);

      return () => {
        window.removeEventListener("keydown", handleKeyDown);
      };
    }, [
      generatedSequence,
      currentScoreIndex,
      includeBarNumbers,
      zoomLevel,
      staffNumberValue,
      loading,
    ]);

    return (
      <main>
        <Box
          onKeyDown={(event) => {
            // Check if the Enter key was pressed
            if (event.key === "Enter" || event.key === "Return") {
              // Prevent handling if the event originated from the Select component
              if (!event.target.classList.contains("level")) {
                handleNextClick();
              }
            }
          }}
          tabIndex={0}
          id="markov"
          sx={{
            mt: -2,
          }}
        />
        <Box
          className="buttons"
          sx={{
            display: "flex",
            justifyContent: "center",
            marginRight: "4em",
            gap: 1,
          }}
        >
          <Select
            className="level"
            value={currentScoreIndex}
            onChange={handleLevelChange}
            sx={{
              "& .MuiSelect-icon": {
                color: "inherit",
              },
            }}
            onClose={() => {
              // https://stackoverflow.com/questions/65411927/blur-material-ui-select-component-on-the-onchange-event
              // Blur Select element after selection
              setTimeout(() => {
                document.activeElement.blur();
              }, 0);
            }}
          >
            {Object.keys(Levels).map((level, index) => (
              <MenuItem key={index} value={index}>
                {level}
              </MenuItem>
            ))}
          </Select>

          <Button
            variant="contained"
            color="secondary"
            onClick={handleNextClick}
          >
            Next <NavigateNextIcon />
          </Button>
        </Box>
      </main>
    );
  },
);

export default Score;
