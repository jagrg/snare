// Settings

import React, { useState } from "react";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import SettingsIcon from "@mui/icons-material/Settings";
import {
  Typography,
  Box,
  Select,
  MenuItem,
  Divider,
  Button,
  Checkbox,
} from "@mui/material";
import FullScreen from "./FullScreen";
import CloseIcon from "@mui/icons-material/Close";
import TooltipHelper from "./TooltipHelper";

import PrintButton from "./PrintButton";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";

const Settings = ({
  numberOfBars,
  onBarsChange,
  onBeatsChange,
  includeBarNumbers,
  onIncludeBarNumbersChange,
  onShowMetronomeChange,
  showMetronome,
  onZoomChange,
  onStaffNumberChange,
}) => {
  const [open, setOpen] = useState(false);
  const [numberOfBeats, setNumberOfBeats] = useState(4);
  const [staffNumberValue, setStaffNumberValue] = useState(false);

  const handleStaffNumberChange = (value) => {
    setStaffNumberValue(value);
    onStaffNumberChange(value);
  };

  // Zoom score
  const handleZoomIn = () => {
    onZoomChange((prevZoom) => Math.max(0.1, prevZoom / 1.1));
  };

  const handleZoomOut = () => {
    onZoomChange((prevZoom) => prevZoom * 1.1);
  };

  // Print score only
  const handlePrint = () => {
    const contentToPrint = document.getElementById("markov");

    if (contentToPrint) {
      // Create an iframe
      const iframe = document.createElement("iframe");
      iframe.style.display = "none";

      // Append it to the body
      document.body.appendChild(iframe);

      // Write the content to the iframe
      const iframeDocument = iframe.contentWindow.document;
      iframeDocument.open();
      iframeDocument.write(`
      <html>
        <head>
          <title>Print ${numberOfBars} bars</title>
        </head>
        <body>
          ${contentToPrint.innerHTML}
        </body>
      </html>
    `);
      iframeDocument.close();

      // Print the iframe
      iframe.contentWindow.print();

      // Remove the iframe from the document after printing
      iframe.parentNode.removeChild(iframe);
    }
  };

  const handleShowMetronomeChange = (value) => {
    onShowMetronomeChange(value);
  };

  const handleCheckboxChange = (event) => {
    const value = event.target.checked;
    onIncludeBarNumbersChange(value);
  };

  const handleDrawerOpen = (event) => {
    setOpen(true);
    event.currentTarget.blur();
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleBarsChange = (event) => {
    const value = event.target.value;
    onBarsChange(value);
    /* handleDrawerClose(); */
  };

  const handleBeatsChange = (event) => {
    const value = event.target.value;
    setNumberOfBeats(value);
    if (onBeatsChange) {
      onBeatsChange(value);
    }
    /* handleDrawerClose(); */
  };

  const handleBlur = () => {
    const focusedElement = document.activeElement;
    if (focusedElement) {
      focusedElement.blur();
    }
  };

  const generateBarOptions = () => {
    const options = [];
    for (let i = 4; i <= 32; i += 2) {
      options.push(
        <MenuItem key={i} value={i}>
          {i}
        </MenuItem>,
      );
    }
    return options;
  };

  const generateBeatOptions = () => {
    const options = [2, 3, 4, 5, 6].map((value) => (
      <MenuItem key={value} value={value}>
        {value}
      </MenuItem>
    ));
    return options;
  };

  return (
    <div>
      <Box
        className="settings"
        sx={{
          display: "flex",
          color: "#fff",
          marginTop: "0.7em",
          marginRight: "0.7em",
          gap: 1,
        }}
      >
        <IconButton
          variant="outlined"
          color="inherit"
          onClick={handleDrawerOpen}
        >
          <SettingsIcon />
        </IconButton>
      </Box>
      <Drawer
        anchor="right"
        open={open}
        onClose={handleDrawerClose}
        sx={{ width: 300 }}
      >
        <Box
          p={3}
          display="flex"
          flexDirection="column"
          alignItems="flex-start"
          onBlur={handleBlur}
          marginBottom="-1.5em"
        >
          <Typography variant="h5" sx={{ marginBottom: "1em" }} gutterBottom>
            Settings
          </Typography>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
            width="100%"
            marginBottom="0.5em"
          >
            <Typography variant="body1" sx={{ marginRight: "60px" }}>
              Zoom:
            </Typography>
            <Button
              sx={{ marginRight: "0.5em" }}
              variant="outlined"
              onClick={handleZoomIn}
            >
              <AddIcon />
            </Button>
            <Button variant="outlined" onClick={handleZoomOut}>
              <RemoveIcon />
            </Button>
          </Box>

          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
            width="100%"
            marginBottom="0.5em"
            marginTop="5px"
          >
            <Typography variant="body1" sx={{ marginRight: "60px" }}>
              Number of bars:
            </Typography>
            <Select value={numberOfBars} onChange={handleBarsChange}>
              {generateBarOptions()}
            </Select>
          </Box>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
            width="100%"
            marginBottom="0.5em"
          >
            <Typography variant="body1" sx={{ marginRight: "60px" }}>
              Beats per bar:
            </Typography>
            <Select value={numberOfBeats} onChange={handleBeatsChange}>
              {generateBeatOptions()}
            </Select>
          </Box>
          <Divider
            sx={{
              width: "100%",
              marginY: 3,
            }}
          />

          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
            width="100%"
            marginBottom="0.5em"
          >
            <TooltipHelper title="Toggle bar numbers" placement="top-start">
              <Checkbox
                color="secondary"
                checked={includeBarNumbers}
                onChange={handleCheckboxChange}
              />
            </TooltipHelper>
            <Typography variant="body1">Bar numbers</Typography>
          </Box>

          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
            width="100%"
            marginBottom="0.5em"
          >
            <TooltipHelper title="Toggle metronome" placement="right">
              <Checkbox
                color="secondary"
                checked={showMetronome}
                onChange={(e) => handleShowMetronomeChange(e.target.checked)}
              />
            </TooltipHelper>
            <Typography variant="body1">Metronome</Typography>
          </Box>

          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
            width="100%"
            marginBottom="0.5em"
          >
            <TooltipHelper
              title="Toggle single-line staff"
              placement="bottom-start"
            >
              <Checkbox
                color="secondary"
                checked={staffNumberValue}
                onChange={(e) => handleStaffNumberChange(e.target.checked)}
              />
            </TooltipHelper>
            <Typography variant="body1">Single-line staff</Typography>
          </Box>

          <Divider
            sx={{
              width: "100%",
              marginY: 2,
            }}
          />
        </Box>
        <FullScreen handleDrawerClose={handleDrawerClose} />
        <IconButton
          edge="end"
          color="inherit"
          onClick={handleDrawerClose}
          sx={{
            position: "absolute",
            top: 0,
            right: 0,
            marginTop: "0.7em",
            marginRight: "0.7em",
          }}
        >
          <CloseIcon />
        </IconButton>
        <PrintButton onClick={handlePrint} />
      </Drawer>
    </div>
  );
};

export default Settings;
