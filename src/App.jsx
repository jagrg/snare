// App.jsx

import React from "react";
import Header from "./Header";
import Instruments from "./Instruments";
import Footer from "./Footer";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";

import { createTheme, ThemeProvider } from "@mui/material/styles";

const primary = {
  main: "#1c1c1b",
  light: "#4c4c4b",
  dark: "#0c0c0b",
  contrastText: "#ffffff",
};

const secondary = {
  main: "#ce4a7e",
  light: "#ff7eb1",
  dark: "#9c005c",
  contrastText: "#ffffff",
};

const theme = createTheme({
  palette: {
    primary: primary,
    secondary: secondary,
  },
});

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <div>
        <Header />
        <Instruments />
        <Footer />
      </div>
    </ThemeProvider>
  );
};

export default App;
