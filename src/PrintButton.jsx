// PrintButton

import React from "react";
import Button from "@mui/material/Button";
import PrintIcon from "@mui/icons-material/Print";

const PrintButton = ({ onClick }) => {
  return (
    <Button
      sx={{ marginTop: "1em" }}
      variant="text"
      color="secondary"
      startIcon={<PrintIcon />}
      onClick={onClick}
    >
      Print
    </Button>
  );
};

export default PrintButton;
