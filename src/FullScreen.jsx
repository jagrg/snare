// FullScreen

import React, { useState, useEffect } from "react";
import Button from "@mui/material/Button";

const FullScreen = ({ handleDrawerClose, handleSelectBlur }) => {
  const [isFullScreen, setIsFullScreen] = useState(
    document.fullscreenElement !== null,
  );

  useEffect(() => {
    const handleFullScreenChange = () => {
      // Update state based on the current full-screen status
      setIsFullScreen(document.fullscreenElement !== null);
    };

    document.addEventListener("fullscreenchange", handleFullScreenChange);

    return () => {
      document.removeEventListener("fullscreenchange", handleFullScreenChange);
    };
  }, []);

  const toggleFullScreen = () => {
    if (!isFullScreen) {
      document.documentElement.requestFullscreen();
    } else {
      document.exitFullscreen();
    }
    handleDrawerClose();
  };

  return (
    <Button
      id="fullscreen"
      variant="text"
      color="secondary"
      onClick={toggleFullScreen}
    >
      {isFullScreen ? "Exit Full Screen" : "Full Screen"}
    </Button>
  );
};

export default FullScreen;
