// Header

import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";

const Header = () => {
  return (
    <Box>
      <AppBar id="app-bar" position="static" sx={{ boxShadow: "none" }}>
        <Toolbar>
          <Button color="inherit">
            <i
              className="fas fa-drum"
              aria-label="drum"
              style={{ fontSize: "1.45rem" }}
            ></i>
          </Button>
          <Typography id="header" variant="h5" style={{ fontWeight: 300 }}>
            Sight-reading for{" "}
            <span style={{ fontWeight: 500, letterSpacing: "-0.45px" }}>
              percussion
            </span>
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
};
export default Header;
