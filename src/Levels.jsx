// Scores based on difficulty levels

// Function to process a level string and generate an array of note objects
const processLevel = (levelString) => {
  const notes = levelString
    .replace(/\|/g, "")
    .split(/\s+/)
    .filter((note) => note.trim() !== "");

  const noteValues = new Map();
  let currentValue = 1;
  const levelArray = [];

  notes.forEach((note) => {
    if (!noteValues.has(note)) {
      noteValues.set(note, currentValue);
      currentValue++;
    }

    levelArray.push({ key: note, value: noteValues.get(note) });
  });

  return levelArray;
};

// Define the level strings using a loop
const levelScores = [
  // Level 1. 1/4 notes on the beats
  `
  c2 c2 c2 z2 | c2 z2 c2 z2 |
  c2 c2 z2 c2 | c2 c2 c2 c2 |
  z2 c2 z2 c2 | c2 c2 c2 z2 |
  c2 z2 c2 c2 | c2 z2 c2 c2 |
  `,

  // Level 2. 1/4 and 1/8 notes on the beats
  `
  c2 c2 cc c2 | cc cc c2 z2 |
  c2 z2 c2 cc | c2 z2 c2 c2 |
  z2 cc c2 cc | cc c2 cc cc |
  c2 z2 cc c2 | c2 z2 c2 z2 |
  `,

  // Level 3. Each beat is divided into equal durations
  `
  c2 cc c/2c/2c/2c/2 c2 | c/2c/2c/2c/2 cc c2 z2 |
  cc cc c/2c/2c/2c/2 cc | c2 z2 cc c/2c/2c/2c/2 c/2c/2c/2c/2 |
  c2 cc c2 c2 | c/2c/2c/2c/2 c2 c/2c/2c/2c/2 cc |
  c2 c/2c/2c/2c/2 c2 c/2c/2c/2c/2 | cc c2 z2 c2 |
  `,

  // Level 4. /6 must occur in pairs replacing an /8 note
  `
  cc/2c/2 c/2c/2c c/2c/2c cc | c/2c/2c cc/2c/2 c2 z2 |
  c/2c/2c/2c/2 c/2c/2c z2 cc/2c/2 | c/2c/2c cc/2c/2 cc c2 |
  z2 c/2c/2c/2c/2 cc/2c/2 cc/2c/2 | c2 c/2c/2c cc c/2c/2c/2c/2 |
  c2 z2 c/2c/2c z2 | z2 c/2c/2c cc/2c/2 c2 |
  `,

  // Level 5. Demo
  `
  c2 cc {\\c}cc (rcc) |
  cc/2c/2 cc zc c/2c/2c |
  (rcc) c/2Lc/2c/2c/2 c{\\c}c z{\\c}c |
  c/2Lc/2c/2c/2 c/2c/2c zc c/2c/2c/2c/2 |

  z/2c/2c/2c/2 z/2c/2c {cc}c2 z2 |
  z/2c/2c/2c/2 c/2c/2{\\c}c zc cc |
  cc/2c/2 c/2c/2c z/2c/2c/2c/2 c/2c/2c/2c/2 |
  c/2c/2c zc/2c/2 {\\c}cc {cc}c2 |
 `,

  // Level 6 TBC
];

// Generate the Levels object
const Levels = levelScores.reduce((acc, levelString, index) => {
  acc[`Level ${index + 1}`] = processLevel(levelString);
  return acc;
}, {});

export default Levels;
