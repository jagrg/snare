// Footer

import React from "react";
import Help from "./Help";
import Link from "@mui/material/Link";
import "./Footer.css";

const Footer = () => {
  return (
    <footer className="footer" role="contentinfo">
      <span className="hspace">
        Built using{" "}
        <Link
          underline="none"
          sx={{ color: "hsl(330, 100%, 75%)" }}
          href="https://react.dev/"
        >
          React
        </Link>{" "}
        and{" "}
        <Link
          underline="none"
          sx={{ color: "hsl(330, 100%, 75%)" }}
          href="https://paulrosen.github.io/abcjs/"
        >
          abcjs
        </Link>
      </span>
      |
      <span className="hspace">
        <Link
          underline="none"
          sx={{ color: "hsl(330, 100%, 75%)" }}
          href="https://git.sr.ht/~jagrg/rhything/tree"
        >
          License
        </Link>
      </span>
      |
      <span className="hspace">
        <Help />
      </span>
    </footer>
  );
};

export default Footer;
