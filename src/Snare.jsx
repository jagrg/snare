// Snare

import React, { useState } from "react";
import Settings from "./Settings";
import Metronome from "./Metronome";
import Score from "./Score";
import Box from "@mui/material/Box";

const Snare = () => {
  const [numberOfBars, setNumberOfBars] = useState(6); // Start with 6 bars
  const [numberOfBeats, setNumberOfBeats] = useState(4); // Start in 4/4
  const [includeBarNumbers, setIncludeBarNumbers] = useState(false); // No numbers by default
  const [showMetronome, setShowMetronome] = useState(true); // Show metronome by default
  const [zoomLevel, setZoomLevel] = useState(1);
  const [staffNumberValue, setStaffNumberValue] = useState(false);

  const handleStaffNumberChange = (value) => {
    setStaffNumberValue(value);
  };

  const handleZoomChange = (value) => {
    setZoomLevel(value);
  };

  const handleMetronomeToggle = (value) => {
    setShowMetronome(value);
  };

  const handleIncludeBarNumbersChange = (value) => {
    setIncludeBarNumbers(value);
  };

  const handleBarsChange = (value) => {
    setNumberOfBars(value);
  };

  const handleBeatsChange = (value) => {
    setNumberOfBeats(value);
  };

  return (
    <div>
      <Settings
        numberOfBars={numberOfBars}
        onBarsChange={handleBarsChange}
        numberOfBeats={numberOfBeats}
        onBeatsChange={handleBeatsChange}
        includeBarNumbers={includeBarNumbers}
        onIncludeBarNumbersChange={handleIncludeBarNumbersChange}
        onShowMetronomeChange={handleMetronomeToggle}
        showMetronome={showMetronome}
        onZoomChange={handleZoomChange}
        onStaffNumberChange={handleStaffNumberChange}
      />
      <Box className="container" sx={{ marginTop: "-3em" }}>
        <Metronome showMetronome={showMetronome} />
        <Box className="vspace" />
        <Score
          numberOfBars={numberOfBars}
          numberOfBeats={numberOfBeats}
          includeBarNumbers={includeBarNumbers}
          zoomLevel={zoomLevel}
          staffNumberValue={staffNumberValue}
        />
      </Box>
    </div>
  );
};

export default Snare;
