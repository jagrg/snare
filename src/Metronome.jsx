// Metronome

import React, { useState, useRef, useEffect } from "react";
import * as Tone from "tone";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import StopIcon from "@mui/icons-material/Stop";
import Slider from "@mui/material/Slider";
import Typography from "@mui/material/Typography";

const Metronome = ({ showMetronome }) => {
  const [bpm, setBpm] = useState(120);
  const [isPlaying, setIsPlaying] = useState(false);
  const [spaceBarPressed, setSpaceBarPressed] = useState(false); // New state for tracking space bar press
  const metronomeRef = useRef();
  const audioContextRef = useRef(null);

  const isPortrait = window.matchMedia("(orientation: portrait)").matches;
  const scoreContainerStyle = {
    // Keep vertical space when Metronome is disabled, but not in portrait mode
    marginTop: !isPortrait ? (showMetronome ? "0" : "2.5em") : "0",
  };

  const membraneSynth = new Tone.MembraneSynth({
    pitchDecay: 0.001,
    octaves: 10,
    oscillator: { type: "sine" },
    envelope: {
      attack: 0.001,
      decay: 0.001,
      sustain: 0.0001,
      release: 0.001,
    },
  }).toDestination();

  const toggleMetronome = () => {
    if (isPlaying) {
      // Stop the Transport and clear the scheduled events
      Tone.Transport.stop();
      Tone.Transport.clear(metronomeRef.current);
    } else {
      // Initialize the audio context on user interaction
      if (audioContextRef.current === null) {
        Tone.start();
        audioContextRef.current = true;
      }

      Tone.Transport.bpm.value = bpm;

      // Schedule the metronome clicks
      metronomeRef.current = Tone.Transport.scheduleRepeat((time) => {
        membraneSynth.triggerAttackRelease("A4", "16n", time);
      }, "4n");

      // Start the Transport
      Tone.Transport.start();
    }

    // Toggle the playing state
    setIsPlaying(!isPlaying);
  };

  // Function to handle BPM change
  const handleBpmChange = (event) => {
    const newBpm = parseInt(event.target.value, 10);
    if (!isNaN(newBpm)) {
      setBpm(newBpm);
      Tone.Transport.bpm.value = newBpm;
    }
  };

  // Function to handle key down events
  const handleKeyDown = (event) => {
    if (event.code === "Space" && !spaceBarPressed) {
      // Prevent default SPC key binding
      event.preventDefault();
      toggleMetronome();
      setSpaceBarPressed(true);
    }
  };

  // Function to handle key up events
  const handleKeyUp = (event) => {
    if (event.code === "Space") {
      setSpaceBarPressed(false);
    }
  };

  useEffect(() => {
    // Clean up the metronome when the component is unmounted
    return () => {
      Tone.Transport.stop();
      Tone.Transport.clear(metronomeRef.current);
    };
  }, []);

  // Attach event listeners for key down and key up
  useEffect(() => {
    window.addEventListener("keydown", handleKeyDown);
    window.addEventListener("keyup", handleKeyUp);

    // Remove event listeners on component unmount
    return () => {
      window.removeEventListener("keydown", handleKeyDown);
      window.removeEventListener("keyup", handleKeyUp);
    };
  }, [spaceBarPressed]);

  return showMetronome ? (
    <Box className="metronome">
      <Typography
        variant="body2"
        color="textSecondary"
        style={{
          padding: "0.5em",
          marginRight: "0.35em",
          fontFamily: "monospace",
          color: "#1c1c1b",
        }}
      >
        {bpm}
      </Typography>
      <Slider
        size="small"
        defaultValue={120}
        color="secondary"
        valueLabelDisplay="off"
        min={25}
        max={250}
        onChange={handleBpmChange}
      />
      <Button
        style={{ fontSize: "0.8rem" }}
        variant="text"
        color="secondary"
        onClick={toggleMetronome}
        sx={{
          minWidth: "0",
          padding: "0.4em",
        }}
      >
        {isPlaying ? (
          <StopIcon style={{ fontSize: 26 }} />
        ) : (
          <PlayArrowIcon style={{ fontSize: 26 }} />
        )}
      </Button>
    </Box>
  ) : (
    <Box style={scoreContainerStyle}></Box>
  );
};

export default Metronome;
