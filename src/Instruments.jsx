// Instruments

import React, { useState } from "react";
import Snare from "./Snare";
import Timpani from "./Timpani";
import Mallets from "./Mallets";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import Box from "@mui/material/Box";

const Instruments = ({ showMetronome, staffNumberValue }) => {
  const [selectedInstrument, setSelectedInstrument] = useState("snare");

  const handleSwitchInstrument = (event) => {
    setSelectedInstrument(event.target.value);
  };

  return (
    <Box>
      <Select
        sx={{
          marginLeft: "0.75rem",
          color: "#555",
          marginBottom: "1em",
          boxShadow: "none",
          ".MuiOutlinedInput-notchedOutline": { border: 0 },
        }}
        disableUnderline={true}
        value={selectedInstrument}
        onChange={handleSwitchInstrument}
        label="Switch instrument"
        variant="standard"
      >
        <MenuItem value="snare">Snare</MenuItem>
        <MenuItem value="timpani">Timpani</MenuItem>
        <MenuItem value="mallets">Mallets</MenuItem>
      </Select>

      {selectedInstrument === "snare" && (
        <Snare
          showMetronome={showMetronome}
          staffNumberValue={staffNumberValue}
        />
      )}
      {selectedInstrument === "timpani" && (
        <Timpani showMetronome={showMetronome} />
      )}
      {selectedInstrument === "mallets" && (
        <Mallets showMetronome={showMetronome} />
      )}
    </Box>
  );
};

export default Instruments;
