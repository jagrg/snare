// Help

import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";

export default function HelpDialog({ onClose }) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = (event) => {
    setOpen(true);

    // Unfocus the last clicked link
    event.currentTarget.blur();
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <Link
        sx={{ color: "hsl(330, 100%, 75%)" }}
        underline="none"
        href="#"
        onClick={handleClickOpen}
      >
        Help
      </Link>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Help"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            This is a progressive sight-reading App that generates new music
            on-demand using the Markov chain model. It is based roughly on
            Percival's work on computer-assisted musical instrument tutoring,
            and provides targeted exercises for percussion. Press{" "}
            <Typography
              component="kbd"
              variant="body2"
              color="textSecondary"
              className="keys"
            >
              Space
            </Typography>{" "}
            to toggle the metronome, and{" "}
            <Typography
              component="kbd"
              variant="body2"
              color="textSecondary"
              className="keys"
            >
              Enter
            </Typography>{" "}
            to change the score.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button color="secondary" onClick={handleClose}>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
